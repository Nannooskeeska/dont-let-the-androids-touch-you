﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class ButtonLoadLevel : MonoBehaviour
{

	void Start ()
	{
		Cursor.visible = true;
		Cursor.lockState = CursorLockMode.Confined;
	}

	public void LoadLevelButton (string levelName)
	{
		SceneManager.LoadScene (levelName);
	}
}
