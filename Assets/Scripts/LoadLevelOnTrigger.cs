﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class LoadLevelOnTrigger : MonoBehaviour
{

	public string levelToLoad;

	void OnTriggerEnter (Collider collider)
	{
		if (levelToLoad != null && collider.tag == "Player") {
			Debug.Log ("Loading " + levelToLoad);
			SceneManager.LoadScene (levelToLoad);
		} else {
			Debug.LogError ("Please enter a level to load");
		}
	}
}
