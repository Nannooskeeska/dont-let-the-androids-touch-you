﻿using UnityEngine;
using System.Collections;

public class GUICrosshair : MonoBehaviour
{
	public Texture2D crosshairTexture;
	private Rect position;
	public bool originalOn = true;

	void Start ()
	{
		position = new Rect ((Screen.width - crosshairTexture.width) / 2, (Screen.height - crosshairTexture.height) / 2, crosshairTexture.width, crosshairTexture.height);
	}

	void onGUI ()
	{
		if (originalOn) {
			GUI.DrawTexture (position, crosshairTexture);
		}
	}
}