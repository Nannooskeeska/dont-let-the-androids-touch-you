﻿using UnityEngine;
using System.Collections;

public class WarpToGameObject : MonoBehaviour
{

	public GameObject warpTo;

	void OnTriggerEnter (Collider collider)
	{
		if (collider.tag == "Player") {
			Debug.Log ("Warping to " + warpTo + " from " + gameObject);
			collider.transform.position = warpTo.transform.position;
		}
	}
}