﻿using UnityEngine;
using System.Collections;

public class FollowPlayer : MonoBehaviour
{

	private GameObject player;
	public float MoveSpeed = 4;
	public float MaxDist = 10;
	public float MinDist = 5;

	void Start ()
	{
		player = GameObject.FindGameObjectWithTag ("Player");
	}

	void Update ()
	{
		transform.LookAt (player.transform);

		if (Vector3.Distance (transform.position, player.transform.position) >= MinDist) {

			transform.position += transform.forward * MoveSpeed * Time.deltaTime;

			if (Vector3.Distance (transform.position, player.transform.position) <= MaxDist) {
				//Here Call any function U want Like Shoot at here or something
			} 
		}
	}
}
