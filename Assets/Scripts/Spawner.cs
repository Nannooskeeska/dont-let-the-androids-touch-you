﻿using UnityEngine;
using System.Collections;

public class Spawner : MonoBehaviour
{

	public GameObject prefabToSpawn;
	public float spawnTime;
	public float spawnTimeRandom;

	private float spawnTimer;
	private GameObject player;

	// Use this for initialization
	void Start ()
	{
		player = GameObject.FindGameObjectWithTag ("Player");
		ResetSpawnTimer ();
	}
	
	// Update is called once per frame
	void Update ()
	{
		spawnTimer -= Time.deltaTime;
		// Make sure the player is at least 20 units away from the spawner
		if (spawnTimer <= 0.0f && Vector3.Distance (player.transform.position, gameObject.transform.position) > 20) {
			Debug.Log ("Distance from player: " + Vector3.Distance (player.transform.position, gameObject.transform.position));
			Instantiate (prefabToSpawn, transform.position, Quaternion.identity);
			ResetSpawnTimer ();
		}
	}

	void ResetSpawnTimer ()
	{
		spawnTimer = (float)(spawnTime + Random.Range (0, spawnTimeRandom * 100) / 100.0);
	}
}
