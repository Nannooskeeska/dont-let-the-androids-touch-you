﻿using UnityEngine;
using System.Collections;

public class RotateObject : MonoBehaviour
{

	public enum RotateAroundAxis
	{
		X,
		Y,
		Z
	}

	public float rotationsPerMinute = 10;
	public RotateAroundAxis localAxis;

	// Use this for initialization
	void Start ()
	{
		
	}
	
	// Update is called once per frame
	void Update ()
	{
		transform.Rotate (0, 0, 6 * rotationsPerMinute * Time.deltaTime);
	}
}
