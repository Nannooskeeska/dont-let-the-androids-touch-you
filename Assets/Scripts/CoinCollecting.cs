﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class CoinCollecting : MonoBehaviour
{

	public AudioClip sound;

	static public float currentCoins = 0;
	static public float levelOneCoins = 0;
	static public float levelTwoCoins = 0;
	static public float levelThreeCoins = 0;
	static public float totalCoins = 0;

	private GUIStyle guiStyle = new GUIStyle ();

	// Gets called every frame, like Update()
	void OnGUI ()
	{
		if (SceneManager.GetActiveScene () == SceneManager.GetSceneByName ("Level1")) {
			GUI.Label (new Rect (Screen.width - 275, 0, 100, 50), "Level 1 Coins: " + levelOneCoins.ToString () + "/10", guiStyle);
		} else if (SceneManager.GetActiveScene () == SceneManager.GetSceneByName ("Level2")) {
			GUI.Label (new Rect (Screen.width - 275, 0, 100, 50), "Level 1 Coins: " + levelOneCoins.ToString () + "/10", guiStyle);
			GUI.Label (new Rect (Screen.width - 275, 40, 100, 50), "Level 2 Coins: " + levelTwoCoins.ToString () + "/10", guiStyle);
		} else if (SceneManager.GetActiveScene () == SceneManager.GetSceneByName ("Level3")) {
			GUI.Label (new Rect (Screen.width - 275, 0, 100, 50), "Level 1 Coins: " + levelOneCoins.ToString () + "/10", guiStyle);
			GUI.Label (new Rect (Screen.width - 275, 40, 100, 50), "Level 2 Coins: " + levelTwoCoins.ToString () + "/10", guiStyle);
			GUI.Label (new Rect (Screen.width - 275, 80, 100, 50), "Level 3 Coins: " + levelThreeCoins.ToString () + "/23", guiStyle);
		} else if (SceneManager.GetActiveScene () == SceneManager.GetSceneByName ("Christmas")) {
			GUI.Label (new Rect (Screen.width - 275, 0, 100, 50), "Level 1 Coins: " + levelOneCoins.ToString () + "/10", guiStyle);
			GUI.Label (new Rect (Screen.width - 275, 40, 100, 50), "Level 2 Coins: " + levelTwoCoins.ToString () + "/10", guiStyle);
			GUI.Label (new Rect (Screen.width - 275, 80, 100, 50), "Level 3 Coins: " + levelThreeCoins.ToString () + "/23", guiStyle);
		}
	}

	void OnTriggerEnter (Collider collider)
	{
		if (collider.tag == "Coin") {
			if (sound != null) {
				AudioSource.PlayClipAtPoint (sound, gameObject.transform.position);
			}

			// Check current scene name and increase number of coins found in that scene
			// if level 1...
			if (SceneManager.GetActiveScene () == SceneManager.GetSceneByName ("Level1")) {
				// increment level 1 coins
				levelOneCoins += 1;
			} else if (SceneManager.GetActiveScene () == SceneManager.GetSceneByName ("Level2")) {
				// increment level 2 coins
				levelTwoCoins += 1;
			} else if (SceneManager.GetActiveScene () == SceneManager.GetSceneByName ("Level3")) {
				// increment level 3 coins
				levelThreeCoins += 1;
			} else {
				Debug.LogError ("This should never happen. If it does, something has gone horribly wrong.");
			}
			totalCoins += 1;
			Destroy (collider.gameObject);
		}
	}

	public float getNumCoins ()
	{
		return currentCoins;
	}
		
	// Use this for initialization
	void Start ()
	{
		guiStyle.fontSize = 30;

	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}
}