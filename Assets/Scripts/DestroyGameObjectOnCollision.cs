﻿using UnityEngine;
using System.Collections;

public class DestroyGameObjectOnCollision : MonoBehaviour
{

	public GameObject explosionPrefab;

	void OnCollisionEnter (Collision collision)
	{
		Destroy (gameObject);
		if (explosionPrefab != null) {
			Instantiate (explosionPrefab, transform.position, transform.rotation);
		}
	}
}