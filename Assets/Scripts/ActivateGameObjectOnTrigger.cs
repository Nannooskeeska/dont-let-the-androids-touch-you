﻿using UnityEngine;
using System.Collections;

public class ActivateGameObjectOnTrigger : MonoBehaviour
{

	public GameObject objectToActivate;

	void OnTriggerEnter (Collider collider)
	{
		if (collider.tag == "Player") {
			if (objectToActivate.activeInHierarchy) {
				Debug.LogError ("Please use a GameObject that is inactive");
			} else {
				Debug.Log ("Activating " + objectToActivate);
				objectToActivate.SetActive (true);
			}
		}
	}
}
