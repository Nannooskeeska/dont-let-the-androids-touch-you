﻿using UnityEngine;
using System.Collections;

public class PlaySoundOnCollision : MonoBehaviour
{

	public AudioClip soundToPlay;

	void OnTriggerEnter (Collider collider)
	{
		if (collider.gameObject.tag == "Player") {
			AudioSource.PlayClipAtPoint (soundToPlay, gameObject.transform.position);
		}
	}
}
